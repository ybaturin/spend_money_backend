import * as Koa from 'koa';
import * as config from 'config';
import * as bodyParser from 'koa-bodyparser';
import * as koaValidator from 'koa-async-validator';
import * as koaCors from 'koa-cors';
import { routes, allowedMethods } from './router/routes';
import errorHandler from './router/error';
import { DB } from "./db";

DB.init();

const app = new Koa();
app.use(koaCors());
app.use(errorHandler);
app.use(bodyParser());
// app.use(koaValidator());
app.use(routes());
app.use(allowedMethods());

const serverPort = process.env.PORT || config.get('server.port');
app.listen(serverPort, () => {
  const appName = config.get('app.name');
  console.log(`${appName} listening at port ${serverPort}`);
});