export interface PaginationResult {
  pagination: Pagination,
  data: any[]
}

export interface Pagination {
  allCount: number
}