export interface Purchase {
  name: string,
  price: number,
  userId: number,
  date: string,
  id: number
}
