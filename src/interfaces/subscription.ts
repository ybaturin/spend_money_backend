export interface Subscription {
  userId: number;
  subscriptionOptions: string;
}
