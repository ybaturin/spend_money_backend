import * as Router from 'koa-router';
import { PurchasesController, Purchase } from "../controllers/purchases.controller";
import {AuthController } from "../controllers/auth.controller";
import { RouterHelper } from "./router-helper";
import { UsersController } from "../controllers/users.controller";
import { PurchasesTestController } from "../tests/purchases-test.controller";
import {Subscription} from "../interfaces/subscription";
import {SubscriptionsController} from "../controllers/subscriptions.controller";

const SUPER_SECRET_TEST_PWD = 'welcometohellmydearly@dv0k@d';

const api = new Router();

api
  .get('/', async(ctx, next) => {
    ctx.body = 'general';
  })
  .get('/test', async(ctx, next) => {
    ctx.body = ctx.query;
    ctx.status = 200;
  })
  .put('/purchase/:id', authCheck, async(ctx, next) => {
    const userId = ctx.state.tokenData.userId;
    const purchase = <Purchase> {
      ...ctx.request.body,
    };

    await PurchasesController.updateOne(userId, purchase);
    ctx.body = {};
    ctx.status = 200;
  })
  .get('/purchase/:id', authCheck, async(ctx, next) => {
    const userId = ctx.state.tokenData.userId;
    const purchaseId = RouterHelper.int(ctx.params.id);

    const purchase = await PurchasesController.getOne(userId, purchaseId);
    ctx.body = purchase;
    ctx.status = 200;
  })
  .delete('/purchase/:id', authCheck, async(ctx, next) => {
    const userId = ctx.state.tokenData.userId;
    const purchaseId = RouterHelper.int(ctx.params.id);

    await PurchasesController.removeOne(userId, purchaseId);
    ctx.body = null;
    ctx.status = 204;
  })
  .get('/day-purchases', authCheck, async(ctx, next) => {
    const userId = ctx.state.tokenData.userId;
    const skip = RouterHelper.int(ctx.query.skip);
    const limit = RouterHelper.int(ctx.query.limit);

    const dayPurchases = await PurchasesController.getAll(userId, skip, limit);
    ctx.body = dayPurchases;
    ctx.status = 200;
  })

  .post('/day-purchases', authCheck, async(ctx, next) => {
    const userId = ctx.state.tokenData.userId;
    const purchase = <Purchase> {
      ...ctx.request.body,
      userId
    };
    const dayPurchases = await PurchasesController.create(purchase);
    ctx.body = dayPurchases;
    ctx.status = 200;
  })
  .post('/authenticate/register', async(ctx, next) => {
    ctx.body = await AuthController.register(ctx.request.body);
    ctx.status = 200;
  })
  .post('/authenticate', async(ctx, next) => {
    ctx.body = await AuthController.auth(ctx.request.body);
    ctx.status = 200;
  })
  .get('/users', authCheck, async(ctx, next) => {
    const userId = ctx.state.tokenData.userId;
    ctx.body = await UsersController.getAll(userId);
    ctx.status = 200;
  })
  .post('/subscription', authCheck, async(ctx, next) => {
    const userId = ctx.state.tokenData.userId;
    const subscriptionOptions = JSON.stringify(ctx.request.body.subscription);
    const subscription = <Subscription>{ userId, subscriptionOptions };
    await SubscriptionsController.createOrUpdate(subscription);
    ctx.body = null;
    ctx.status = 204;
  })
  .post('/subscription/test-push', testCheck, async(ctx, next) => {
      await SubscriptionsController.sendTestPush();
      ctx.body = null;
      ctx.status = 204;
  })
  .post('/test/purchases', authCheck, testCheck, async(ctx, next) => {
    const userId = ctx.state.tokenData.userId;
    const params = ctx.request.body;
    ctx.body = await PurchasesTestController.createPurchases(userId, params.daysCount);
    ctx.status = 200;
  })
;

async function authCheck(ctx, next) {
  const token = ctx.request.body.token || ctx.request.headers['x-access-token'];
  let isError = false;
  try {
    const tokenData = await AuthController.decodeToken(token);
    ctx.state.tokenData = tokenData;
  } catch (error) {
    ctx.body = { message: 'Invalid token' };
    ctx.status = 401;
    ctx.type = 'application/javascript';
    isError = true;
  }

  if (!isError) {
    await next();
  }
}

async function testCheck(ctx, next) {
  const pwd = ctx.request.headers['x-text-pwd'];
  if (pwd !== SUPER_SECRET_TEST_PWD) {
    ctx.body = { message: 'Invalid token' };
    ctx.status = 401;
    ctx.type = 'application/javascript';
  } else {
    await next();
  }
}

function routes() { return api.routes() };
function allowedMethods() { return api.allowedMethods() };

export { routes, allowedMethods }