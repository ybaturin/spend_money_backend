import { IncorrectLoginOrPassException, NotAllowedUserException, UserAlreadyExistException } from "./exceptions";

export default async (ctx, next) => {
  try {
    await next();
  } catch (error) {
    console.log(error);

    if (error instanceof NotAllowedUserException) {
      ctx.body = { message: 'Current user hasn\'t rights for this operation' };
      ctx.status = 403;
    } else if (error instanceof UserAlreadyExistException) {
      ctx.body = { message: 'User already exist' };
      ctx.status = 409;
    } else if (error instanceof IncorrectLoginOrPassException) {
      ctx.body = { message: 'Login/password is incorrect or user doesn\'t exist' };
      ctx.status = 401;
    } else if (error.name === 'ValidationError') {
      ctx.status = 400;
      ctx.body = {
        message: error.errors,
      };
    } else {
      ctx.status = 500;
      ctx.body = {
        message: error.message,
        stack: error.stack
      };
    }
  }

  ctx.type = 'application/javascript'
}