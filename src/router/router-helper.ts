class Helper {
  int(value) {
    const parsed = parseInt(value, 10);
    return isNaN(parsed) ? undefined: parsed;
  }
}

const RouterHelper = new Helper();
export { RouterHelper };