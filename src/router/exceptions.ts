export class UserAlreadyExistException extends Error {}
export class IncorrectLoginOrPassException extends Error {}
export class NotAllowedUserException extends Error {}

