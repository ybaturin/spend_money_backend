import * as Sequelize from 'sequelize';
import * as config from 'config';
import { Subscription } from 'src/interfaces/subscription';

class DBManager {
  private sequelize;
  private uri: string;

  public User: Sequelize.Model<any, any>;
  public Purchases: Sequelize.Model<any, any>;
  public SharedUser: Sequelize.Model<any, any>;
  public Subscriptions: Sequelize.Model<Subscription, any>;

  init() {
    this.uri = process.env.CLEARDB_DATABASE_URL || <string>config.get('db.uri');
    this.sequelize = new Sequelize(this.uri, <Sequelize.Options>{
      logging: <boolean>config.get('db.logging'),
    });
    this._initModels();
    this._checkConnection();
  }

  _initModels() {
    this.User = this.sequelize.define('user', <Sequelize.DefineAttributes>{
      login: <Sequelize.DefineAttributeColumnOptions>{ type: Sequelize.STRING, allowNull: false, unique: true },
      password: <Sequelize.DefineAttributeColumnOptions>{ type: Sequelize.STRING, allowNull: false },
      name: <Sequelize.DefineAttributeColumnOptions>{ type: Sequelize.STRING, allowNull: false },
      avatar: <Sequelize.DefineAttributeColumnOptions>{ type: Sequelize.STRING, allowNull: true },
    });

    this.Subscriptions = this.sequelize.define('subscriptions', <Sequelize.DefineAttributes>{
      subscriptionOptions: <Sequelize.DefineAttributeColumnOptions>{ type: Sequelize.STRING(2000), allowNull: false },
    });

    this.SharedUser = this.sequelize.define('shareduser');

    this.Purchases = this.sequelize.define('purchase', <Sequelize.DefineAttributes>{
      name: <Sequelize.DefineAttributeColumnOptions>{ type: Sequelize.STRING(2000), allowNull: false },
      price: <Sequelize.DefineAttributeColumnOptions>{ type: Sequelize.INTEGER, allowNull: false },
      date: <Sequelize.DefineAttributeColumnOptions>{
        type: Sequelize.DATEONLY,
        allowNull: false,
        defaultValue: Sequelize.NOW
      }
    }, <Sequelize.DefineOptions<Object>>{
      indexes: [
        {  fields: ['date'] }
      ]
    });

    this.Purchases.belongsTo(this.User);
    this.Subscriptions.belongsTo(this.User);
    this.User.hasMany(this.Purchases);
    this.User.hasMany(this.Subscriptions);

    // this.User.belongsToMany(this.User, {
    //   as: 'shared',
    //   foreignKey: 'sourceUserId',
    //   through: this.SharedUser
    // });
    this.User.belongsToMany(this.User, {
      as: 'shared',
      otherKey: 'sourceUserId',
      foreignKey: 'sharedUserId',
      through: this.SharedUser
    });

    // this.User.belongsToMany(this.User, {
    //   as: 'shared',
    //   foreignKey: 'sourceUserId',
    //   through: this.SharedUser
    // });
    // this.User.belongsToMany(this.User, {
    //   as: 'shared',
    //   foreignKey: 'targetUserId',
    //   through: this.SharedUser
    // });
    // this.SharedUser.hasMany(this.User, { foreignKey: 'targetUserId' });
    // this.User.belongsToMany(this.User, {
    //   as: 'shared',
    //   foreignKey: 'targetUserId',
    //   otherKey: 'sourceUserId',
    //   through: 'sharedusers'
    // });
    // this.User.belongsToMany(this.User, { as: 'shared', foreignKey: 'targetUserId', through: this.SharedUser });

    this.sequelize.sync();
  }

  async _checkConnection() {
    try {
      await this.sequelize.authenticate();
      console.log('Connected to DB: ', this.uri)
    } catch (error) {
      console.error('Can\'t connect to DB: ', error.message)
    }
  }
}

const DB = new DBManager();
export { DB };