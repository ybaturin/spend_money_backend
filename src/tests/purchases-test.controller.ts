import * as Sequelize from 'sequelize';
import { DB } from "../db/index";
import * as _ from 'lodash';
import * as moment from 'moment';
import { PurchasesController, Purchase } from "../controllers/purchases.controller";
import { UsersController } from "../controllers/users.controller";
import { PaginationResult } from "../interfaces/pagination-result";

const PURCHASES_NAMES = [
  'джинсы Ярику (из детских денег)',
  'заказ из Германии',
  'подгузники',
  'катание на лошадке',
  'абонемент в БЕГОВЕЛиК для Ярика',
];

class PurchasesTest {
  async createPurchases(userId: number, daysCount = 30) : Promise<PaginationResult> {
    const dates = [];
    const purchases = [];
    const currentDate = moment();
    for (let i = 0; i < daysCount; i++) {
      const date = currentDate.clone().add(-i, 'days');
      dates.push(date.format('YYYY-MM-DD'));
    }

    const users = await UsersController.getAll(userId);
    const userIds = users.map(user => user.id);

    dates.forEach((date) => {
      userIds.forEach(async (userId) => {
        const maxPurchases = _.random(1, 10);
        for (let i = 0; i < maxPurchases; i++) {
          const name = PURCHASES_NAMES[_.random(0, PURCHASES_NAMES.length - 1)];
          const price = _.random(-9999, 9999);

          const purchase = <Purchase> { name, price, userId, date };
          purchases.push(purchase);
        }
      });
    });

    for (let purchase of purchases) {
      await PurchasesController.create(purchase);
    }

    return PurchasesController.getAll(userIds[0], 0, daysCount);
  }
}

const PurchasesTestController = new PurchasesTest();

export { PurchasesTestController };