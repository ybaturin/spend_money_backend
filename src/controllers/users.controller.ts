import { DB } from "../db/index";
import { User } from "../interfaces/user";

class Users {
  async getAll(userId: number) : Promise<User[]> {
    const user = await DB.User.findOne({
      where: { id: { $eq: userId }}
    });
    const users = await user.getShared({ attributes: ['id', 'name', 'avatar']});

    const result = users.map((user) => {
      return <User> {
        name: user.name,
        avatar: user.avatar,
        id: user.id
      }
    });

    result.push(<User> {
      name: user.name,
      avatar: user.avatar,
      id: user.id
    });

    return result;
  }
}

const UsersController = new Users();

export { UsersController };