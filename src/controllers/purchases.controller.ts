import * as Sequelize from 'sequelize';
import * as _ from 'lodash';
import { DB } from "../db/index";
import { UsersController } from "./users.controller";
import { Purchase } from "../interfaces/purchase";
import { PaginationResult, Pagination } from "../interfaces/pagination-result";
import { NotAllowedUserException } from "../router/exceptions";
import {SubscriptionsController} from "./subscriptions.controller";

class Purchases {
  async create(purchase: Purchase) {
    const result = await DB.Purchases.create(purchase);
    SubscriptionsController.sendPurchasePush(purchase.userId, purchase);
    return result;
  }

  async getAll(userId: number, skip = 0, limit = 5) : Promise<PaginationResult> {
    const result = <PaginationResult> {
      pagination: <Pagination> { allCount: 0 },
      data: []
    };

    const dates = await DB.Purchases.findAll({
      attributes: ['date'],
      order: [['date', 'DESC']],
      group: ['date']
    });

    if (dates.length === 0 || skip >= dates.length) {
      return result;
    }

    const users = await UsersController.getAll(userId);
    const userIds = users.map(user => user.id);

    const startDate = dates[skip].get('date');
    const endDateIdx = Math.min(limit + skip, dates.length) - 1;
    const endDate = dates[endDateIdx].get('date');

    const purchases = <Purchase[]>(await DB.Purchases.findAll({
      order: [['date', 'DESC']],
      where: {
        date: {
          $lte: startDate,
          $gte: endDate
        },
        userId: {
          $in: userIds
        }
      }
    }));

    result.pagination.allCount = dates.length;
    result.data = purchases;

    return result;
  }

  async _checkUserRights(currentUserId: number, checkUserId: number) {
    const users = await UsersController.getAll(currentUserId);
    const userIds = users.map(user => user.id);

    if (_.includes(userIds, checkUserId)) {
      throw new NotAllowedUserException();
    }
  }

  async getOne(userId: number, purchaseId: number) : Promise<Purchase> {
    const purchase = await DB.Purchases.findById(purchaseId);
    if (purchase) {
      this._checkUserRights(userId, purchase.userId);
    }

    return purchase;
  }

  async removeOne(userId: number, purchaseId: number) : Promise<void> {
    await this.getOne(userId, purchaseId);
    await DB.Purchases.destroy({
      where: { id : purchaseId }
    })
  }

  async updateOne(userId: number, purchase: Purchase) {
    await this.getOne(userId, purchase.id);
    await DB.Purchases.update(purchase, {
      where: { id : purchase.id }
    })
  }
}

const PurchasesController = new Purchases();

export { PurchasesController, Purchase };