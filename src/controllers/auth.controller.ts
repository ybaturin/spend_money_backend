import { DB } from "../db/index";
import * as passwordHash from 'password-hash';
import * as config from 'config';
import * as jwt from 'jsonwebtoken';
import { User } from "../interfaces/user";
import { IncorrectLoginOrPassException, UserAlreadyExistException } from "../router/exceptions";

const TOKEN_SECRET = <string>config.get('token.secret');
const TOKEN_EXPIRES_MIN = <string>config.get('token.expires');

class Auth {
  async register(userData: ICreateUser): Promise<IAuthResult> {
    const password = passwordHash.generate(userData.password);
    const userDataPass = {
      ...userData,
      password
    };

    const existUser = await DB.User.findOne({
      attributes: ['id'],
      where: { login: userData.login }
    });
    if (existUser) {
      throw new UserAlreadyExistException();
    }

    const userDb = await DB.User.create(userDataPass);
    return this._getAuthResult(userDb);
  }

  _getToken(user) {
    const tokenData = <ITokenData>{ userId: user.id };
    return jwt.sign(tokenData, TOKEN_SECRET, {
      expiresIn: TOKEN_EXPIRES_MIN
    });
  }

  _getAuthResult(userDb: any): IAuthResult {
    return <IAuthResult> {
      token: this._getToken(userDb),
      user: <User> {
        name: userDb.name,
        avatar: userDb.avatar,
        id: userDb.id
      }
    }
  }

  async decodeToken(token): Promise<ITokenData> {
    return new Promise<ITokenData>((resolve, reject) => {
      if (!token) {
        reject();
      } else {
        jwt.verify(token, TOKEN_SECRET, (err, decoded) => {
          if (err) {
            reject(null);
          } else {
            resolve(decoded);
          }
        });
      }
    });
  }
  async auth(userData: IAuthUser): Promise<IAuthResult> {
    const userDb = await DB.User.findOne({
      where: { login: userData.login }
    });

    if (!userDb || !passwordHash.verify(userData.password, userDb.password)) {
      throw new IncorrectLoginOrPassException();
    }

    return this._getAuthResult(userDb)
  }
}

interface ICreateUser {
  login: string,
  password: string,
  name: string,
  avatar: string
}

interface IAuthUser {
  login: string,
  password: string,
  name: string,
  avatar: string
}

interface ITokenData {
  userId: string
}

interface IAuthResult {
  token: string,
  user: User
}

const AuthController = new Auth();

export { AuthController, ICreateUser };