import { DB } from "../db/index";
import {Subscription} from "../interfaces/subscription";
import * as webpush from 'web-push';
import {Purchase} from "../interfaces/purchase";
import {Hash} from "../interfaces/hash";

const SEND_MESSAGE_DELAY = 1000 * 60 * 2;

interface DelayMessage {
  purchases: Purchase[];
  timeout?: NodeJS.Timer;
}

enum SubscriptionType {
  purchases = 'purchases',
}

interface PushMessage {
  type: SubscriptionType;
  [prop: string]: any
}

interface PurchasesPushMessage extends Purchase {
  type: SubscriptionType.purchases;
  purchases: Purchase[];
}

class Subscriptions {
  private delayMessages: Hash<DelayMessage> = {};

  async createOrUpdate(subscription: Subscription) {
    const result = await DB.Subscriptions.findOne({
      where: {
        userId: subscription.userId,
        subscriptionOptions: subscription.subscriptionOptions,
      }
    });

    if (result) {
      return DB.Subscriptions.update(subscription, {
        where: {
          userId: subscription.userId,
          subscriptionOptions: subscription.subscriptionOptions,
        }
      });
    } else {
      return DB.Subscriptions.create(subscription);
    }
  }

  async sendTestPush() {
    const result = await DB.Subscriptions.findAll();
    // const sub = JSON.parse(result[0].subscriptionOptions);

    const options = {
      gcmAPIKey: 'AAAAHxBXp8k:APA91bEvi1kAKca5-IoRejLLs_agXdaJRGOqN5GJXUpqxzcFLLFST3s5jf_fZunJB-OJklSxUcUrTrdWfb0jDJ8TRn--xVZpmmTLHi13V49KRBTwIleGJFDMvu8fney-u6YPLuRbvF3K',
      vapidDetails: {
        subject: 'http://spendmoney-front.herokuapp.com',
        publicKey: "BB8HUyP5PTgm9oS9AZ4m6UY4czMPkpPT5Y9FEd0HGH-F6K1uTXT2X8VNErAEuagrdF1bGNMgQtussSMi1sjXSQI",
        privateKey: "euKwEiNxRh7biNiLVlQFNwVKaIX6cBVuTUV0UL16CpY"
      },
    };
    // const sub = {"endpoint":"https://fcm.googleapis.com/fcm/send/cJIpnwUMR4M:APA91bFI9kJYwUh-8hIwoX4JeSb_ZG4OV_Fn3vZBvwIvfpmbBydBf-AyBW3FOL7XBv7PqhTdlsdRWVHmjrq9vuj5XXNVmoj4YXhIKeUx91EPK1DqzMRi2jhlzaVFLM5kQm0gsFt-ZqUu","keys":{"p256dh":"BNJ5t8WFOFO0H374U3JDqk5pdzJYv092XKmUYr24sPdzGZXIbcASP339IcH_3meHvGCQ6k9zpPPEYvMPCdA7QbY=","auth":"W-wDx6vG80A1788iwsJ-7g=="}};
    //android
    const sub = {"endpoint":"https://fcm.googleapis.com/fcm/send/dLSbLOWMHvY:APA91bEFn3Ou1uQX1luH5pOqch-iXfAtkb8D_uDQ0iSoxfC4xtuQ669C9bbX11BE0Tz1KqTXjCjo1yqQP4DyI6SW1WRLKcADu2I8bypcwi-2QJGgKsZlt3OHzFT1dRgNfvasp60qnPM1","expirationTime":null,"keys":{"p256dh":"BFlmdADvY_60ANij7LDW_IlkoXOgFhg4zROD7_w_54jlkUVCQoxzVAKdxzacVQ9V7mhRTUsYmCD0uBQZZN8p4zs=","auth":"XlAdvoA7If_zMYWHigua9A=="}};

    const purchasesAll = await DB.Purchases.findAll();
    const purchases = purchasesAll.slice(-15);

    try {
      await this.sendPushMessage([sub], 'test', {
        type: SubscriptionType.purchases,
        purchases: purchases,
      })
    } catch (error) {
      console.log(error);
    }
  }

  async sendPurchasePush(initUserId: number, purchase: Purchase) {
    let message = this.delayMessages[initUserId] as DelayMessage;
    if (message) {
      clearTimeout(message.timeout);
    } else {
      message = { purchases: [] };
      this.delayMessages[initUserId] = message;
    }
    message.purchases.push(purchase);
    message.timeout = setTimeout(() => this.sendPurchasePushImmediately(initUserId), SEND_MESSAGE_DELAY);
  }

  private async sendPurchasePushImmediately(initUserId: number) {
    try {
      const subs = await this.getSharedUserSubscriptions(initUserId);
      const message = this.delayMessages[initUserId];
      this.delayMessages[initUserId] = null;
      const push = <PurchasesPushMessage>{
        type: SubscriptionType.purchases,
        purchases: message.purchases,
      };
      await this.sendPushMessage(subs, 'spendmoney', push);
    } catch (error) {
      console.error(error);
    }
  }

  private async getSharedUserSubscriptions(userId: number) {
    const user = await DB.User.findOne({
      where: { id: { $eq: userId }}
    });
    const users = await user.getShared({ attributes: ['id']});
    const userIds = users.map(user => user.id);

    const subscriptions = await DB.Subscriptions.findAll({
      where: {
        userId: {
          $in: userIds
        }
      }
    }).map((data:any) => JSON.parse(data.subscriptionOptions));
    return subscriptions;
  }

  private async sendPushMessage(subscriptions:any[], title:string, message: PushMessage) {
    // TODO вынести в конфиг
    const options = {
      gcmAPIKey: 'AAAAHxBXp8k:APA91bEvi1kAKca5-IoRejLLs_agXdaJRGOqN5GJXUpqxzcFLLFST3s5jf_fZunJB-OJklSxUcUrTrdWfb0jDJ8TRn--xVZpmmTLHi13V49KRBTwIleGJFDMvu8fney-u6YPLuRbvF3K',
      vapidDetails: {
        subject: 'http://spendmoney-front.herokuapp.com',
        publicKey: "BB8HUyP5PTgm9oS9AZ4m6UY4czMPkpPT5Y9FEd0HGH-F6K1uTXT2X8VNErAEuagrdF1bGNMgQtussSMi1sjXSQI",
        privateKey: "euKwEiNxRh7biNiLVlQFNwVKaIX6cBVuTUV0UL16CpY"
      },
    };

    const data = {
      ...message,
      siteUrl: 'https://spendmoney-front.herokuapp.com/purchases',
    };

    const payload = JSON.stringify({
      notification: {
        title,
        data,
        icon: 'assets/img/logo-256.png',
        badge: 'assets/img/logo-256.png',
        vibrate: [400,300,200,100,80],
      }
    });

    subscriptions.forEach(sub => webpush.sendNotification(sub, payload, options));
  }
}

const SubscriptionsController = new Subscriptions();

export { SubscriptionsController };