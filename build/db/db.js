"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const Sequelize = require("sequelize");
const config = require("config");
class DBManager {
    init() {
        this.uri = process.env.CLEARDB_DATABASE_URL || config.get('db.uri');
        this.sequelize = new Sequelize(this.uri);
        this._initModels();
        this._checkConnection();
    }
    _initModels() {
        this.User = this.sequelize.define('user', {
            username: { type: Sequelize.STRING, allowNull: false },
            avatar: { type: Sequelize.STRING, allowNull: true },
        });
        this.Purchases = this.sequelize.define('purchase', {
            name: { type: Sequelize.STRING, allowNull: false },
            price: { type: Sequelize.INTEGER, allowNull: false },
            date: {
                type: Sequelize.DATEONLY,
                allowNull: false,
                defaultValue: Sequelize.NOW
            }
        }, {
            indexes: [
                { fields: ['date'] }
            ]
        });
        this.Purchases.belongsTo(this.User);
        this.User.hasMany(this.Purchases);
        this.sequelize.sync();
    }
    _checkConnection() {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                yield this.sequelize.authenticate();
                console.log('Connected to DB: ', this.uri);
            }
            catch (error) {
                console.error('Can\'t connect to DB: ', error.message);
            }
        });
    }
}
const DB = new DBManager();
exports.DB = DB;
