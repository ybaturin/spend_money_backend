"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const db_1 = require("./db");
class DBHolderClass {
    init() {
        this.db = new db_1.default();
    }
    get() {
        return this.db;
    }
}
const DBHolder = new DBHolderClass();
exports.DBHolder = DBHolder;
