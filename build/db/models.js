"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const mongoose = require("mongoose");
const day_purchases_1 = require("./schemes/day-purchases");
const user_1 = require("./schemes/user");
const UserModel = mongoose.model('user', user_1.UserSchema);
exports.UserModel = UserModel;
const DayPurchasesModel = mongoose.model('day_purchases', day_purchases_1.DayPurchasesSchema);
exports.DayPurchasesModel = DayPurchasesModel;
