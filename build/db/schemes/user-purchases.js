"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const mongoose = require("mongoose");
const idValidator = require("mongoose-id-validator");
const purchases_1 = require("./purchases");
const Schema = mongoose.Schema;
const UserPurchasesSchema = new Schema({
    user: {
        type: Schema.Types.ObjectId,
        ref: 'user',
        required: true,
    },
    purchases: [purchases_1.PurchasesSchema]
});
exports.UserPurchasesSchema = UserPurchasesSchema;
UserPurchasesSchema.plugin(idValidator);
