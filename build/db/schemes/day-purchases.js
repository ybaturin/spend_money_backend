"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const mongoose = require("mongoose");
const moment = require("moment");
const user_purchases_1 = require("./user-purchases");
const Schema = mongoose.Schema;
const DayPurchasesSchema = new Schema({
    date: {
        type: Date,
        required: true,
        default: () => {
            return moment().startOf('day');
        }
    },
    userPurchases: [user_purchases_1.UserPurchasesSchema]
});
exports.DayPurchasesSchema = DayPurchasesSchema;
