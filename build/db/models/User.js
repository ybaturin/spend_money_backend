"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const Sequelize = require("sequelize");
// const UserDef = {
//   username: { type: Sequelize.STRING, allowNull: false },
//   avatar: { type: Sequelize.STRING, allowNull: true },
// };
//
// interface IUserModel {
//   username: string,
//   avatar: string
// };
function UserDef(sequelize) {
    sequelize.define('User', {
        username: { type: Sequelize.STRING, allowNull: false },
        avatar: { type: Sequelize.STRING, allowNull: true },
    });
}
exports.UserDef = UserDef;
