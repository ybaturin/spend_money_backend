"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const Sequelize = require("sequelize");
function PurchasesModel(sequelize) {
    sequelize.define('User', {
        username: { type: Sequelize.STRING, allowNull: false },
        avatar: { type: Sequelize.STRING, allowNull: true },
    });
}
exports.PurchasesModel = PurchasesModel;
