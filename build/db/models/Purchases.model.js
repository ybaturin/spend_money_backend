"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const Sequelize = require("sequelize");
function PurchasesModel(sequelize) {
    sequelize.define('Purchases', {
        name: { type: Sequelize.STRING, allowNull: false },
        price: { type: Sequelize.INTEGER, allowNull: false },
    });
}
exports.PurchasesModel = PurchasesModel;
