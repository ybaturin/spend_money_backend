"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const index_1 = require("../db/index");
const users_controller_1 = require("./users.controller");
class Create {
    create(purchase) {
        return __awaiter(this, void 0, void 0, function* () {
            return index_1.DB.Purchases.create(purchase);
        });
    }
    getAll(userId, skip = 0, limit = 5) {
        return __awaiter(this, void 0, void 0, function* () {
            const dates = yield index_1.DB.Purchases.findAll({
                attributes: ['id', 'date'],
                order: [['date', 'DESC']],
                group: ['date']
            });
            if (dates.length === 0 || skip > dates.length) {
                return [];
            }
            const users = yield users_controller_1.UsersController.getAll(userId);
            const userIds = users.map(user => user.id);
            console.log(userIds);
            const startDate = dates[skip].get('date');
            const endDateIdx = Math.min(limit + skip, dates.length) - 1;
            const endDate = dates[endDateIdx].get('date');
            const purchases = yield index_1.DB.Purchases.findAll({
                order: [['date', 'DESC']],
                where: {
                    date: {
                        $lte: startDate,
                        $gte: endDate
                    },
                    userId: {
                        $in: userIds
                    }
                }
            });
            return purchases;
        });
    }
}
const PurchasesController = new Purchases();
exports.PurchasesController = PurchasesController;
