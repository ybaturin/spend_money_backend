"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const _ = require("lodash");
const moment = require("moment");
const purchases_controller_1 = require("../controllers/purchases.controller");
const users_controller_1 = require("../controllers/users.controller");
const PURCHASES_NAMES = [
    'джинсы Ярику (из детских денег)',
    'заказ из Германии',
    'подгузники',
    'катание на лошадке',
    'абонемент в БЕГОВЕЛиК для Ярика',
];
class PurchasesTest {
    createPurchases(userId, daysCount = 30) {
        return __awaiter(this, void 0, void 0, function* () {
            const dates = [];
            const purchases = [];
            const currentDate = moment();
            for (let i = 0; i < daysCount; i++) {
                const date = currentDate.clone().add(-i, 'days');
                dates.push(date.format('YYYY-MM-DD'));
            }
            const users = yield users_controller_1.UsersController.getAll(userId);
            const userIds = users.map(user => user.id);
            dates.forEach((date) => {
                userIds.forEach((userId) => __awaiter(this, void 0, void 0, function* () {
                    const maxPurchases = _.random(1, 10);
                    for (let i = 0; i < maxPurchases; i++) {
                        const name = PURCHASES_NAMES[_.random(0, PURCHASES_NAMES.length - 1)];
                        const price = _.random(-9999, 9999);
                        const purchase = { name, price, userId, date };
                        purchases.push(purchase);
                    }
                }));
            });
            for (let purchase of purchases) {
                yield purchases_controller_1.PurchasesController.create(purchase);
            }
            return purchases_controller_1.PurchasesController.getAll(userIds[0], 0, daysCount);
        });
    }
}
const PurchasesTestController = new PurchasesTest();
exports.PurchasesTestController = PurchasesTestController;
