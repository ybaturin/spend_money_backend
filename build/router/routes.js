"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const Router = require("koa-router");
const purchases_controller_1 = require("../controllers/purchases.controller");
const auth_controller_1 = require("../controllers/auth.controller");
const router_helper_1 = require("./router-helper");
const users_controller_1 = require("../controllers/users.controller");
const purchases_test_controller_1 = require("../tests/purchases-test.controller");
const subscriptions_controller_1 = require("../controllers/subscriptions.controller");
const SUPER_SECRET_TEST_PWD = 'welcometohellmydearly@dv0k@d';
const api = new Router();
api
    .get('/', (ctx, next) => __awaiter(this, void 0, void 0, function* () {
    ctx.body = 'general';
}))
    .get('/test', (ctx, next) => __awaiter(this, void 0, void 0, function* () {
    ctx.body = ctx.query;
    ctx.status = 200;
}))
    .put('/purchase/:id', authCheck, (ctx, next) => __awaiter(this, void 0, void 0, function* () {
    const userId = ctx.state.tokenData.userId;
    const purchase = Object.assign({}, ctx.request.body);
    yield purchases_controller_1.PurchasesController.updateOne(userId, purchase);
    ctx.body = {};
    ctx.status = 200;
}))
    .get('/purchase/:id', authCheck, (ctx, next) => __awaiter(this, void 0, void 0, function* () {
    const userId = ctx.state.tokenData.userId;
    const purchaseId = router_helper_1.RouterHelper.int(ctx.params.id);
    const purchase = yield purchases_controller_1.PurchasesController.getOne(userId, purchaseId);
    ctx.body = purchase;
    ctx.status = 200;
}))
    .delete('/purchase/:id', authCheck, (ctx, next) => __awaiter(this, void 0, void 0, function* () {
    const userId = ctx.state.tokenData.userId;
    const purchaseId = router_helper_1.RouterHelper.int(ctx.params.id);
    yield purchases_controller_1.PurchasesController.removeOne(userId, purchaseId);
    ctx.body = null;
    ctx.status = 204;
}))
    .get('/day-purchases', authCheck, (ctx, next) => __awaiter(this, void 0, void 0, function* () {
    const userId = ctx.state.tokenData.userId;
    const skip = router_helper_1.RouterHelper.int(ctx.query.skip);
    const limit = router_helper_1.RouterHelper.int(ctx.query.limit);
    const dayPurchases = yield purchases_controller_1.PurchasesController.getAll(userId, skip, limit);
    ctx.body = dayPurchases;
    ctx.status = 200;
}))
    .post('/day-purchases', authCheck, (ctx, next) => __awaiter(this, void 0, void 0, function* () {
    const userId = ctx.state.tokenData.userId;
    const purchase = Object.assign({}, ctx.request.body, { userId });
    const dayPurchases = yield purchases_controller_1.PurchasesController.create(purchase);
    ctx.body = dayPurchases;
    ctx.status = 200;
}))
    .post('/authenticate/register', (ctx, next) => __awaiter(this, void 0, void 0, function* () {
    ctx.body = yield auth_controller_1.AuthController.register(ctx.request.body);
    ctx.status = 200;
}))
    .post('/authenticate', (ctx, next) => __awaiter(this, void 0, void 0, function* () {
    ctx.body = yield auth_controller_1.AuthController.auth(ctx.request.body);
    ctx.status = 200;
}))
    .get('/users', authCheck, (ctx, next) => __awaiter(this, void 0, void 0, function* () {
    const userId = ctx.state.tokenData.userId;
    ctx.body = yield users_controller_1.UsersController.getAll(userId);
    ctx.status = 200;
}))
    .post('/subscription', authCheck, (ctx, next) => __awaiter(this, void 0, void 0, function* () {
    const userId = ctx.state.tokenData.userId;
    const subscriptionOptions = JSON.stringify(ctx.request.body.subscription);
    const subscription = { userId, subscriptionOptions };
    yield subscriptions_controller_1.SubscriptionsController.createOrUpdate(subscription);
    ctx.body = null;
    ctx.status = 204;
}))
    .post('/subscription/test-push', testCheck, (ctx, next) => __awaiter(this, void 0, void 0, function* () {
    yield subscriptions_controller_1.SubscriptionsController.sendTestPush();
    ctx.body = null;
    ctx.status = 204;
}))
    .post('/test/purchases', authCheck, testCheck, (ctx, next) => __awaiter(this, void 0, void 0, function* () {
    const userId = ctx.state.tokenData.userId;
    const params = ctx.request.body;
    ctx.body = yield purchases_test_controller_1.PurchasesTestController.createPurchases(userId, params.daysCount);
    ctx.status = 200;
}));
function authCheck(ctx, next) {
    return __awaiter(this, void 0, void 0, function* () {
        const token = ctx.request.body.token || ctx.request.headers['x-access-token'];
        let isError = false;
        try {
            const tokenData = yield auth_controller_1.AuthController.decodeToken(token);
            ctx.state.tokenData = tokenData;
        }
        catch (error) {
            ctx.body = { message: 'Invalid token' };
            ctx.status = 401;
            ctx.type = 'application/javascript';
            isError = true;
        }
        if (!isError) {
            yield next();
        }
    });
}
function testCheck(ctx, next) {
    return __awaiter(this, void 0, void 0, function* () {
        const pwd = ctx.request.headers['x-text-pwd'];
        if (pwd !== SUPER_SECRET_TEST_PWD) {
            ctx.body = { message: 'Invalid token' };
            ctx.status = 401;
            ctx.type = 'application/javascript';
        }
        else {
            yield next();
        }
    });
}
function routes() { return api.routes(); }
exports.routes = routes;
;
function allowedMethods() { return api.allowedMethods(); }
exports.allowedMethods = allowedMethods;
;
