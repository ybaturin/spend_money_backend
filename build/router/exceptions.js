"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
class UserAlreadyExistException extends Error {
}
exports.UserAlreadyExistException = UserAlreadyExistException;
class IncorrectLoginOrPassException extends Error {
}
exports.IncorrectLoginOrPassException = IncorrectLoginOrPassException;
class NotAllowedUserException extends Error {
}
exports.NotAllowedUserException = NotAllowedUserException;
