"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const exceptions_1 = require("./exceptions");
exports.default = (ctx, next) => __awaiter(this, void 0, void 0, function* () {
    try {
        yield next();
    }
    catch (error) {
        console.log(error);
        if (error instanceof exceptions_1.NotAllowedUserException) {
            ctx.body = { message: 'Current user hasn\'t rights for this operation' };
            ctx.status = 403;
        }
        else if (error instanceof exceptions_1.UserAlreadyExistException) {
            ctx.body = { message: 'User already exist' };
            ctx.status = 409;
        }
        else if (error instanceof exceptions_1.IncorrectLoginOrPassException) {
            ctx.body = { message: 'Login/password is incorrect or user doesn\'t exist' };
            ctx.status = 401;
        }
        else if (error.name === 'ValidationError') {
            ctx.status = 400;
            ctx.body = {
                message: error.errors,
            };
        }
        else {
            ctx.status = 500;
            ctx.body = {
                message: error.message,
                stack: error.stack
            };
        }
    }
    ctx.type = 'application/javascript';
});
