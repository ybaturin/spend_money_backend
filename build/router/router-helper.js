"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
class Helper {
    int(value) {
        const parsed = parseInt(value, 10);
        return isNaN(parsed) ? undefined : parsed;
    }
}
const RouterHelper = new Helper();
exports.RouterHelper = RouterHelper;
