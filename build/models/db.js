"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const mongoose = require("mongoose");
const config = require("config");
class DB {
    connect() {
        const dbUri = config.get('db.mongoose.uri');
        mongoose.connect(dbUri);
        this.db = mongoose.connection;
        this.db.on('error', function (err) {
            console.error('connection error:', err.message);
        });
        this.db.once('open', function callback() {
            console.info("Connected to DB!");
        });
    }
}
exports.default = DB;
