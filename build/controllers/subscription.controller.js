"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const _ = require("lodash");
const index_1 = require("../db/index");
const users_controller_1 = require("./users.controller");
const exceptions_1 = require("../router/exceptions");
class Subscriptions {
    create(purchase) {
        return __awaiter(this, void 0, void 0, function* () {
            return index_1.DB.Subscriptions.create(purchase);
        });
    }
    getAll(userId, skip = 0, limit = 5) {
        return __awaiter(this, void 0, void 0, function* () {
            const result = {
                pagination: { allCount: 0 },
                data: []
            };
            const dates = yield index_1.DB.Subscriptions.findAll({
                attributes: ['date'],
                order: [['date', 'DESC']],
                group: ['date']
            });
            if (dates.length === 0 || skip >= dates.length) {
                return result;
            }
            const users = yield users_controller_1.UsersController.getAll(userId);
            const userIds = users.map(user => user.id);
            const startDate = dates[skip].get('date');
            const endDateIdx = Math.min(limit + skip, dates.length) - 1;
            const endDate = dates[endDateIdx].get('date');
            const Subscriptions = (yield index_1.DB.Subscriptions.findAll({
                order: [['date', 'DESC']],
                where: {
                    date: {
                        $lte: startDate,
                        $gte: endDate
                    },
                    userId: {
                        $in: userIds
                    }
                }
            }));
            result.pagination.allCount = dates.length;
            result.data = Subscriptions;
            return result;
        });
    }
    _checkUserRights(currentUserId, checkUserId) {
        return __awaiter(this, void 0, void 0, function* () {
            const users = yield users_controller_1.UsersController.getAll(currentUserId);
            const userIds = users.map(user => user.id);
            if (_.includes(userIds, checkUserId)) {
                throw new exceptions_1.NotAllowedUserException();
            }
        });
    }
    getOne(userId, purchaseId) {
        return __awaiter(this, void 0, void 0, function* () {
            const purchase = yield index_1.DB.Subscriptions.findById(purchaseId);
            if (purchase) {
                this._checkUserRights(userId, purchase.userId);
            }
            return purchase;
        });
    }
    removeOne(userId, purchaseId) {
        return __awaiter(this, void 0, void 0, function* () {
            yield this.getOne(userId, purchaseId);
            yield index_1.DB.Subscriptions.destroy({
                where: { id: purchaseId }
            });
        });
    }
    updateOne(userId, purchase) {
        return __awaiter(this, void 0, void 0, function* () {
            yield this.getOne(userId, purchase.id);
            yield index_1.DB.Subscriptions.update(purchase, {
                where: { id: purchase.id }
            });
        });
    }
}
const PurchasesController = new Subscriptions();
exports.PurchasesController = PurchasesController;
