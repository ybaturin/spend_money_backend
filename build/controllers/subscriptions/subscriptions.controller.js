"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const index_1 = require("../../db/index");
const webpush = require("web-push");
class Subscriptions {
    createOrUpdate(subscription) {
        return __awaiter(this, void 0, void 0, function* () {
            const result = yield index_1.DB.Subscriptions.findOne({
                where: {
                    userId: subscription.userId,
                    subscriptionOptions: subscription.subscriptionOptions,
                }
            });
            if (result) {
                return index_1.DB.Subscriptions.update(subscription, {
                    where: {
                        userId: subscription.userId,
                        subscriptionOptions: subscription.subscriptionOptions,
                    }
                });
            }
            else {
                return index_1.DB.Subscriptions.create(subscription);
            }
        });
    }
    sendTestPush() {
        return __awaiter(this, void 0, void 0, function* () {
            const result = yield index_1.DB.Subscriptions.findAll();
            // const sub = JSON.parse(result[0].subscriptionOptions);
            const options = {
                gcmAPIKey: 'AAAAHxBXp8k:APA91bEvi1kAKca5-IoRejLLs_agXdaJRGOqN5GJXUpqxzcFLLFST3s5jf_fZunJB-OJklSxUcUrTrdWfb0jDJ8TRn--xVZpmmTLHi13V49KRBTwIleGJFDMvu8fney-u6YPLuRbvF3K',
                vapidDetails: {
                    subject: 'http://spendmoney-front.herokuapp.com',
                    publicKey: "BB8HUyP5PTgm9oS9AZ4m6UY4czMPkpPT5Y9FEd0HGH-F6K1uTXT2X8VNErAEuagrdF1bGNMgQtussSMi1sjXSQI",
                    privateKey: "euKwEiNxRh7biNiLVlQFNwVKaIX6cBVuTUV0UL16CpY"
                },
            };
            // const sub = {"endpoint":"https://fcm.googleapis.com/fcm/send/cJIpnwUMR4M:APA91bFI9kJYwUh-8hIwoX4JeSb_ZG4OV_Fn3vZBvwIvfpmbBydBf-AyBW3FOL7XBv7PqhTdlsdRWVHmjrq9vuj5XXNVmoj4YXhIKeUx91EPK1DqzMRi2jhlzaVFLM5kQm0gsFt-ZqUu","keys":{"p256dh":"BNJ5t8WFOFO0H374U3JDqk5pdzJYv092XKmUYr24sPdzGZXIbcASP339IcH_3meHvGCQ6k9zpPPEYvMPCdA7QbY=","auth":"W-wDx6vG80A1788iwsJ-7g=="}};
            //android
            const sub = { "endpoint": "https://fcm.googleapis.com/fcm/send/cQp8LEuQofM:APA91bG3ABDO-zkCZ3VLWTdrZzSSp4El9PqsMQdiSb5yJRT56WjCxMvvbKSf7KDF2F1XwKNi1vin2U0CEoNz1zMRLpz-Hhe6d9lhlQ49BiqGFsH4nPrzu9K06LjEHS6JcvcoWloyV1JF", "expirationTime": null, "keys": { "p256dh": "BAyRvSmAJCjCW4nOL63UMUbiWj3yiUL7W-cVBumNQ-R_cWmenNixZdMPU--dCHVpfArsBxscbp4xqfLzq5l2bPI=", "auth": "kyPnOOZKwaldxzHVsN-brA==" } };
            // const payload = JSON.stringify({
            //   notification: {
            //     title: "Я заголовок",
            //     body: "Интересно, как много текста может\n вместится сюда сюда\n\n сюда сюда <b>сюда</b> сюда сюда сюда сюда сюда сюда сюда сюда сюда сюда сюда сюда сюда сюда сюда сюда сюда сюда сюда сюда сюда сюда сюда сюда сюда сюда сюда сюда сюда сюда сюда сюда сюда сюда сюда",
            //     icon: "assets/img/logo-256.png",
            //     data: { siteUrl: 'https://spendmoney-front.herokuapp.com/purchases' },
            //     badge: "assets/img/logo-256.png",
            //     vibrate: [400,300,200,100,80],
            //   }
            // });
            const payload = JSON.stringify({
                notification: {
                    title: "Были добавлены расходы:",
                    icon: "assets/img/logo-256.png",
                    data: {
                        isPurchases: true,
                        purchases: {
                            spentMoney: 1200,
                            receivedMoney: 0,
                        },
                        siteUrl: 'https://spendmoney-front.herokuapp.com/purchases',
                    },
                    badge: "assets/img/logo-256.png",
                    vibrate: [400, 300, 200, 100, 80],
                }
            });
            // const payload = 'test';
            webpush.sendNotification(sub, payload, options);
        });
    }
    sendPurchasePush(initUserId, purchase) {
        return __awaiter(this, void 0, void 0, function* () {
            const subs = yield this.getSharedUserSubscriptions(initUserId);
            const purchasePush = {
                spentMoney: 0,
                receivedMoney: 0,
            };
            if (purchase.price < 0) {
                purchasePush.spentMoney = Math.abs(purchase.price);
            }
            else {
                purchasePush.receivedMoney = purchase.price;
            }
            const data = {
                isPurchases: true,
                purchases: purchasePush,
            };
            this.sendPushMessage(subs, 'Были добавлены расходы:', data);
        });
    }
    getSharedUserSubscriptions(userId) {
        return __awaiter(this, void 0, void 0, function* () {
            const user = yield index_1.DB.User.findOne({
                where: { id: { $eq: userId } }
            });
            const users = yield user.getShared({ attributes: ['id'] });
            const userIds = users.map(user => user.id);
            const subscriptions = yield index_1.DB.Subscriptions.findAll({
                where: {
                    userId: {
                        $in: userIds
                    }
                }
            }).map((data) => JSON.parse(data.subscriptionOptions));
            return subscriptions;
        });
    }
    sendDelayPush(initUserId, message) {
        return __awaiter(this, void 0, void 0, function* () {
        });
    }
    sendPushMessage(subscriptions, title, inputData) {
        return __awaiter(this, void 0, void 0, function* () {
            // TODO вынести в конфиг
            const options = {
                gcmAPIKey: 'AAAAHxBXp8k:APA91bEvi1kAKca5-IoRejLLs_agXdaJRGOqN5GJXUpqxzcFLLFST3s5jf_fZunJB-OJklSxUcUrTrdWfb0jDJ8TRn--xVZpmmTLHi13V49KRBTwIleGJFDMvu8fney-u6YPLuRbvF3K',
                vapidDetails: {
                    subject: 'http://spendmoney-front.herokuapp.com',
                    publicKey: "BB8HUyP5PTgm9oS9AZ4m6UY4czMPkpPT5Y9FEd0HGH-F6K1uTXT2X8VNErAEuagrdF1bGNMgQtussSMi1sjXSQI",
                    privateKey: "euKwEiNxRh7biNiLVlQFNwVKaIX6cBVuTUV0UL16CpY"
                },
            };
            const data = Object.assign({}, inputData, { siteUrl: 'https://spendmoney-front.herokuapp.com/purchases' });
            const payload = JSON.stringify({
                notification: {
                    title,
                    data,
                    icon: 'assets/img/logo-256.png',
                    badge: 'assets/img/logo-256.png',
                    vibrate: [400, 300, 200, 100, 80],
                }
            });
            subscriptions.forEach(sub => webpush.sendNotification(sub, payload, options));
        });
    }
}
const SubscriptionsController = new Subscriptions();
exports.SubscriptionsController = SubscriptionsController;
