"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const index_1 = require("../db/index");
const passwordHash = require("password-hash");
const config = require("config");
const jwt = require("jsonwebtoken");
const exceptions_1 = require("../router/exceptions");
const TOKEN_SECRET = config.get('token.secret');
const TOKEN_EXPIRES_MIN = config.get('token.expires');
class Auth {
    register(userData) {
        return __awaiter(this, void 0, void 0, function* () {
            const password = passwordHash.generate(userData.password);
            const userDataPass = Object.assign({}, userData, { password });
            const existUser = yield index_1.DB.User.findOne({
                attributes: ['id'],
                where: { login: userData.login }
            });
            if (existUser) {
                throw new exceptions_1.UserAlreadyExistException();
            }
            const userDb = yield index_1.DB.User.create(userDataPass);
            return this._getAuthResult(userDb);
        });
    }
    _getToken(user) {
        const tokenData = { userId: user.id };
        return jwt.sign(tokenData, TOKEN_SECRET, {
            expiresIn: TOKEN_EXPIRES_MIN
        });
    }
    _getAuthResult(userDb) {
        return {
            token: this._getToken(userDb),
            user: {
                name: userDb.name,
                avatar: userDb.avatar,
                id: userDb.id
            }
        };
    }
    decodeToken(token) {
        return __awaiter(this, void 0, void 0, function* () {
            return new Promise((resolve, reject) => {
                if (!token) {
                    reject();
                }
                else {
                    jwt.verify(token, TOKEN_SECRET, (err, decoded) => {
                        if (err) {
                            reject(null);
                        }
                        else {
                            resolve(decoded);
                        }
                    });
                }
            });
        });
    }
    auth(userData) {
        return __awaiter(this, void 0, void 0, function* () {
            const userDb = yield index_1.DB.User.findOne({
                where: { login: userData.login }
            });
            if (!userDb || !passwordHash.verify(userData.password, userDb.password)) {
                throw new exceptions_1.IncorrectLoginOrPassException();
            }
            return this._getAuthResult(userDb);
        });
    }
}
const AuthController = new Auth();
exports.AuthController = AuthController;
