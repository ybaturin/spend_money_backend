"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const models_1 = require("../db/models");
const mongoose = require("mongoose");
const moment = require("moment");
const _ = require("lodash");
class PurchasesMongoController {
    create(purchase) {
        return __awaiter(this, void 0, void 0, function* () {
            const today = moment().startOf('day');
            const tomorrow = today.clone().add(1, 'day').startOf('day');
            let dayPurch = yield models_1.DayPurchasesModel.findOne({
                date: { $gte: today, $lt: tomorrow }
            });
            const objUserId = mongoose.Types.ObjectId(purchase.userId);
            if (!dayPurch) {
                // первая запись о покупках в этот день для всех поль-лей
                dayPurch = new models_1.DayPurchasesModel({ userPurchases: [] });
            }
            let userPurchase = _.find(dayPurch.userPurchases, userPurch => userPurch.user.equals(objUserId));
            if (!userPurchase) {
                userPurchase = {
                    user: objUserId,
                    purchases: []
                };
                dayPurch.userPurchases.push(userPurchase);
            }
            userPurchase.purchases.push({ name: purchase.name, price: purchase.price });
            let saveError;
            yield dayPurch.save(error => saveError = error);
            if (saveError) {
                throw saveError;
            }
            yield models_1.DayPurchasesModel.populate(dayPurch, { path: 'userPurchases.user' });
            return dayPurch;
        });
    }
    getAll(skip = 0, limit = 5) {
        return models_1.DayPurchasesModel.find().skip(skip).limit(limit);
    }
}
exports.PurchasesMongoController = PurchasesMongoController;
