"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const Router = require("koa-router");
const api = new Router();
api
    .get('/', (ctx, next) => __awaiter(this, void 0, void 0, function* () {
    // ctx.body = purchases.getAll()
    ctx.body = 'general';
}))
    .get('/purchases', (ctx, next) => __awaiter(this, void 0, void 0, function* () {
    // ctx.body = purchases.getAll()
    ctx.body = 'purchases';
}))
    .get('/put', (ctx, next) => __awaiter(this, void 0, void 0, function* () {
}));
function routes() { return api.routes(); }
exports.routes = routes;
;
function allowedMethods() { return api.allowedMethods(); }
exports.allowedMethods = allowedMethods;
;
